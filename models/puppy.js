var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var schema = new Schema({
  name: { type: String, required: true },
  age: { type: String, required: true },
  category: { type: String, required: true },
  sex: { type: String, required: true },
  price: { type: String, required: true },
  deposit: { type: String, required: true },
  eye: { type: String, required: true },
  breed: { type: String, required: true, default: "French Bulldog" },
  description: { type: String, required: true },
  available: { type: String, required: true },
  status: { type: String, required: true },
  images: { type: Array, required: true }
});

module.exports = mongoose.model("Puppy", schema);
