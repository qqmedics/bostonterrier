var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var schema = new Schema({
  name: { type: String, required: true },
  city: { type: String, required: true },
  state: { type: String, required: true },
  isActive: { type: String, required: true, default: true },
  image: { type: String, required: true },
  message: { type: String, required: true }
});

module.exports = mongoose.model("Testimony", schema);
