module.exports = function cart(oldCart) {
  this.items = oldCart.items || {};
  this.totalQty = oldCart.totalQty || 0;
  this.totalPrice = oldCart.totalPrice || 0;
  this.totalDeposit = oldCart.totalDeposit || 0;
  this.totalShipping = oldCart.totalShipping || 0;

  this.add = function(item, id) {
    var storedItem = this.items[id];
    if (!storedItem) {
      storedItem = this.items[id] = {
        item: item,
        qty: 0,
        price: 0,
        amt: 0
      };
    }
    storedItem.qty++;
    storedItem.price = storedItem.item.price * storedItem.qty;
    storedItem.amt = storedItem.item.amount * storedItem.qty;
    this.totalDeposit += parseInt(storedItem.item.deposit);
    this.totalQty++;
    this.totalPrice += parseInt(storedItem.item.price);
  };
  this.reduceByOne = function(id) {
    this.items[id].qty--;
    this.items[id].price -= this.items[id].item.price;
    this.items[id].amt -= this.items[id].item.amount;
    this.totalQty--;
    this.totalPrice -= this.items[id].item.price;
    this.totalDeposit -= this.items[id].item.deposit;

    if (this.items[id].qty <= 0) {
      delete this.items[id];
    }
  };

  this.removeItem = function(id) {
    this.totalDeposit -= this.items[id].deposit;
    this.totalQty -= this.items[id].qty;
    this.totalPrice -= this.items[id].price;
    delete this.items[id];
  };

  this.generateArray = function() {
    var arr = [];
    for (var id in this.items) {
      arr.push(this.items[id]);
    }
    return arr;
  };
};
