const express = require("express");
const router = express.Router();
const Cart = require("../models/cart");
const Puppy = require("../models/puppy");
const Testimonials = require("../models/testimonials");

const multer = require("multer");

const fs = require("fs");
// var upload = multer({ dest: "public/images/uploads" });
var keys = require("../config/keys");

var storage = multer.diskStorage({
  destination: function(req, file, cb) {
    cb(null, "public/images/uploads");
  },
  filename: function(req, file, cb) {
    cb(null, "puppy-" + file.originalname);
  }
});
var upload = multer({ storage: storage });

/* GET home page. */
router.get("/", function(req, res, next) {
  var messages = req.flash("success");
  Puppy.find()
    .sort("title")
    .then(function(docs) {
      res.render("shop/index", {
        title: "Welcome To crownfrawns",
        puppies: docs,
        messages: messages,
        hasErrors: messages.length > 0,
        type: "success"
      });
    });
});

// update function
let updatz = function(upid, Puppy) {
  Puppy.findByIdAndUpdate(
    upid,
    { $set: { status: "Sold" } },
    { new: true },
    function(err, puppy) {
      if (err) return handleError(err);
      return true;
    }
  );
};

// get add puppy form
router.get("/add-puppy/", function(req, res) {
  res.render("shop/add-puppy", { title: "Add new puppy to shop" });
});

// get Contact FQA
router.get("/contact-faqs", function(req, res) {
  res.render("shop/contact-faq", { title: "Contact US & FAQs" });
});

// Get Testimonnial
router.get("/testimonials", function(req, res) {
  Testimonials.find({ isActive: true }).then(function(docs) {
    res.render("shop/testimonials", {
      title: "Testimonials",
      testimonials: docs
    });
  });
});

// Get buyer contract

router.get("/buyers-contract", function(req, res) {
  res.render("shop/buyercontract", { title: "Buyers Contract" });
});
router.get("/docs/crown-frawns-buyers-contract", function(req, res) {
  var filePath = __dirname + "/docs/Crawn-Frawns-Buyers-Contract.pdf";
  console.log(filePath);
  fs.readFile(filePath, function(err, data) {
    if (err) {
      console.log({ err });
    }
    res.contentType("application/pdf");
    res.send(data);
  });
});

//Add Puppy
router.post("/add-puppy-now/", upload.array("files"), function(req, res) {
  let images = [];
  req.files.forEach(file => {
    let f = "puppy-" + file.originalname;
    images.push(f);
  });
  let data = {
    name: req.body.name,
    category: req.body.category,
    status: req.body.status,
    eye: req.body.eye,
    sex: req.body.sex,
    age: req.body.age,
    available: req.body.available,
    price: req.body.price,
    deposit: req.body.deposit,
    description: req.body.description,
    images
  };
  var puppy = new Puppy(data);
  puppy.save(err => {
    if (err) {
      req.flash("danger", "Something Went Wrong TRY AGAIN");
      res.redirect("/add-puppy");
      console.log(err);
    } else {
      req.flash("success", "New Puppy Added");
      res.redirect("/");
    }
  });
});

// Add Testimony
router.post("/add-testimonial", upload.single("image"), function(req, res) {
  let image = "puppy-" + req.file.originalname;
  let data = {
    name: req.body.name,
    city: req.body.city,
    state: req.body.state,
    image: image,
    message: req.body.message
  };
  var testimony = new Testimonials(data);
  testimony.save(err => {
    if (err) {
      req.flash("danger", "Something Went Wrong TRY AGAIN");
      res.redirect("/testtimonials");
      console.log(err);
    } else {
      req.flash("success", "Testimony submitted");
      res.redirect("/");
    }
  });
});

//Add Items to Cart
router.get("/add-to-cart/:id/", function(req, res) {
  var puppyId = req.params.id;
  var cart = new Cart(req.session.cart ? req.session.cart : {});

  Puppy.findById(puppyId, function(err, puppy) {
    if (err) {
      return res.send("ERR" + err);
    }
    cart.add(puppy, puppy.id);
    req.session.cart = cart;
    // console.log(req.session.cart);
    res.send(req.session.cart);
  });
});

//Get shopping cart Page
router.get("/shopping-cart/", function(req, res, next) {
  if (!req.session.cart) {
    return res.render("shop/shopping-cart", {
      title: "Shopping Cart",
      puppies: null
    });
  }

  var cart = new Cart(req.session.cart);

  let toBePaid = {
    amount: cart.totalPrice,
    type: "deposit"
  };

  if (toBePaid.type == "deposit") {
    toBePaid.amount == cart.totalDeposit;
  }

  var messages = req.flash("danger");
  res.render("shop/shopping-cart", {
    title: "Shopping Cart",
    puppies: cart.generateArray(),
    totalPrice: cart.totalPrice,
    toBePaid: toBePaid,
    totalDeposit: cart.totalDeposit,
    messages: messages,
    hasErrors: messages.length > 0,
    type: "danger"
  });
});

//Get Remove ByOne
router.get("/reduce/:id/", function(req, res, next) {
  var puppyId = req.params.id;
  var cart = new Cart(req.session.cart ? req.session.cart : {});
  cart.reduceByOne(puppyId);
  req.session.cart = cart;
  res.redirect("/shopping-cart");
});

//Get Remove all Page
router.get("/remove/:id/", function(req, res, next) {
  var puppyId = req.params.id;
  var cart = new Cart(req.session.cart ? req.session.cart : {});
  cart.removeItem(puppyId);
  req.session.cart = cart;
  res.redirect("/shopping-cart");
});

// get puppy
router.get("/puppy/:id/", function(req, res) {
  Puppy.findById(req.params.id, function(err, item) {
    if (err) {
      console.log(err);
    } else {
      res.render("shop/puppy", {
        title: item.title,
        item: item
      });
    }
  });
});

// get select puppy
router.get("/frenchies/:category/", function(req, res) {
  var messages = req.flash("success");
  let category = req.params.category;
  Puppy.find({ category: category }).then(function(docs) {
    if (category === "studs" || category === "females") {
      res.render("shop/parents", {
        title: category,
        puppies: docs,
        messages: messages,
        hasErrors: messages.length > 0,
        type: "success"
      });
    } else if (category === "past-puppies") {
      res.render("shop/past", {
        title: category,
        puppies: docs,
        messages: messages,
        hasErrors: messages.length > 0,
        type: "success"
      });
    } else {
      res.render("shop/puppies", {
        title: category,
        puppies: docs,
        messages: messages,
        hasErrors: messages.length > 0,
        type: "success"
      });
    }
  });
});

router.post("/charge/", function(req, res) {
  res.render("shop/order", {
    title: "Delivery Address",
    orderItems: req.body.orderItems
  });
});

module.exports = router;
