var express = require("express");
var path = require("path");
var favicon = require("serve-favicon");
var logger = require("morgan");
var cookieParser = require("cookie-parser");
var bodyParser = require("body-parser");
var session = require("express-session");
var validator = require("express-validator");
var flash = require("connect-flash");
var messages = require("express-messages");
var mongoose = require("mongoose");
var expressHbs = require("express-handlebars");
var passport = require("passport");
var MongoStore = require("connect-mongo")(session);

var index = require("./routes/index");
var users = require("./routes/users");
var order = require("./routes/order");

var app = express();

// MongoDB connection
// var url = "mongodb://localhost:27017/crownfrawns";
let url =
  "mongodb://crownfrawns:#crownfrawns17@cluster0-shard-00-00-ikwws.mongodb.net:27017,cluster0-shard-00-01-ikwws.mongodb.net:27017,cluster0-shard-00-02-ikwws.mongodb.net:27017/test?ssl=true&replicaSet=Cluster0-shard-0&authSource=admin&retryWrites=true&w=majority";
// var url = "mongodb://admin:#crownfrawns17@ds221258.mlab.com:21258/shopping";

mongoose.Promise = global.Promise;

var promise = mongoose.connect(url, { useNewUrlParser: true });

promise.then(
  function(db) {
    console.log("Connected to database!!!");
  },
  function(err) {
    console.log("Error in connecting database " + err);
  }
);

const hbs = expressHbs.create({
  defaultLayout: "layout",
  extname: ".hbs",

  // helpers
  helpers: {
    morf: function(test, yes, no) {
      return test == "Male" ? yes : no;
    },
    trunc: function(passedString) {
      var theString = passedString.substring(0, 150) + " . . .";
      return theString;
    },
    pup_age: function(date1) {
      dt1 = new Date(date1);
      dt2 = new Date();
      difsTime = Math.abs(dt1.getTime() - dt2.getTime());
      difsDays = Math.ceil(difsTime / (1000 * 60 * 60 * 24));
      dayz = difsDays % 7;
      weekz = Math.floor(difsDays / 7);
      if (dayz > 0) {
        word = dayz > 1 ? "days" : "day";
        return `${weekz} Weeks ${dayz} ${word} old`;
      } else {
        return `${weekz} Weeks old`;
      }
    },
    sold: function(es) {
      if (es != "Available") {
        return "disabled";
      }
    },
    status: function(state) {
      return state == "Available" ? "success" : "danger";
    },
    amt_left: function(amt_to_be_paid, total) {
      return total - amt_to_be_paid;
    }
  }
});

// view engine setup
app.engine(".hbs", hbs.engine);
app.set("view engine", ".hbs");

// uncomment after placing your favicon in /public
app.use(favicon(path.join(__dirname, "public", "favicon.ico")));
app.use(logger("dev"));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(validator());
app.use(cookieParser());
app.use(
  session({
    secret: "crownfrawnssecret",
    resave: false,
    saveUninitialized: false,
    store: new MongoStore({ mongooseConnection: mongoose.connection }),
    cookie: { maxAge: 180 * 60 * 1000 }
  })
);
app.use(flash());

app.use(function(req, res, next) {
  res.locals.messages = require("express-messages")(req, res);
  res.locals.session = req.session;
  next();
});

app.use(express.static(path.join(__dirname, "public")));

app.use("/", index);
app.use("/users", users);
app.use("/order", order);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error("Not Found");
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get("env") === "development" ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render("error");
});

module.exports = app;
